eo[0] = "1:{Enviar|Mandar} 2:Juan"
eo[1] = "3:Juan 4:Amigo"
eo[2] = "5:Mandar 6:Algo"
eo[3] = ""
eo[4] = ""
eo[5] = ""
eo[6] = ""

st = "{1|5}:Mandar {2|3}:Juan 4:Amigo"

"""
Paso 1:
Agarra al azar alguna de las palabras, sup {1|5}:Mandar...
Comienza con una ambiguedad, asi que forkea y crea dos ramas.


1) "1:Mandar {2|3}:Juan 4:Amigo"
Agarra ahora no al azar, sino donde esta parado.
Lee el 1, asi que va a hacia ese argumento, e intenta
hinchar. Se encuentra con {, asi que forkea ({ en realidad no
es una letra, sino un link a una estructura con doble link)
Apila lo segundo (Mandar) en alguna especie de stack. Ahora sigue y se da cuenta que
E != M asi que empieza a volver

2) "5:Mandar {2|3}:Juan 4:Amigo"

"""
