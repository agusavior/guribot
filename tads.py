# -*- coding: cp1252 -*-
import re

DEFS = """
1. {Mandar, M, U} -> A
2. {A, D} -> A
3. {H, M} -> D
4. {en, Hs, Ms} -> D
5. {D} -> A
6. {Amigo, U} -> U
"""

ARGS = 0
GOAL = 1
COSTOPORIGNORAR = 0.1

"""
IDEAS PARA MEJORAR LA PERFORMANCE:
1) Que no se copie la lista words todo el tiempo con words[:]
en su lugar que use un puntero para indicar hasta donde llega.
2) Hacer el grafo
"""

"""
class Function:
    def __init__(self, args, end):
        self.args = args
        self.end = end
"""

class EO:
    def __init__(self, *args):
        self.args = args

    #Fill listargs with
    def fill(self, listargs, )

class EOmay(EO): pass
class EOfix(EO): pass
class EOor(EO): pass
class EOset(EO): pass

class GroupOfFunctions: #Change to GraphOfFunctions
    def __init__(self):
        temp = re.findall("{(.*)} -> (.*)", DEFS)
        defs = []
        for a, b in temp:
            defs.append((a.split(", "), b))
        self.functions = defs
    
    #Return a list of functions that each function contains tipo
    def born_in(self, tipo):
        return range(len(self.functions)) #Optimizar

"""
class SetOfFunctions:
    def __init__(self):
        self.functions = set()
        self.actual = None #actual function
    
    def expand(self, otherlist):
        for f in otherlist:
            self.functions.add(f)

    def copy_without_first():
        copy = SetOfFunctions()
        copy.functions = set
"""

class AllStates:
    def __init__(self):
        self.states = [] #Ordenada dependiendo el costo
        self.lastcosto = 0
        self.outputs = []
        self.gof = GroupOfFunctions()
    
    #return_minimal_expression_with_minimal_cost
    def generator(self):
        while self.states != []:
            last = self.states.pop()
            last.step(self)
            while self.outputs != []:
                yield self.outputs.pop()
            
            
    def add(self, state):
        self.states.append(state)

        #Hundir
        for i in reversed(range(len(self.states) - 1)):
            if (self.states[i].costo < self.states[i + 1].costo):
                #Swap
                temp = self.states[i]
                self.states[i] = self.states[i + 1]
                self.states[i + 1] = temp
            else:
                break

    def add_output(self, output):
        self.outputs.append(output)

#En el futuro, cambiar por lista enlazada
class State:
    def __init__(self, goal, news = []):
        self.words = []
        self.news = news
        self.costo = 0
        self.setoffunctions = set()
        self.actual = None #actual function
        self.index = 0 #index on words
        self.indexf = 0 #index on actual function
        self.goal = goal

    def step(self, allstates):
        print str(self)
        if self.news != []:
            self.words = self.words[:self.index] + self.news + self.words[self.index:]
            
            for n in self.news:
                for f in allstates.gof.born_in(n):
                    self.setoffunctions.add(f)
            self.news = []
        elif len(self.words) == 1 and words[0] == goal:
            allstates.add_output(str(self.words)) #¿Cambiar la forma de terminar?
            return #Corta la ejecucion de esta rama
        elif self.actual == None:
            if self.setoffunctions == set():
                return #cortar ejecucion de esta rama
            else:
                self.actual = allstates.gof.functions[self.setoffunctions.pop()]
        elif self.indexf == len(self.actual[ARGS]):
            #Termino de conseguir todos los argumentos, ahora ejecutar
            self.news = [self.actual[GOAL],]

            #Ademas, resetear para sacar el siguiente
            self.actual = None
            self.index = 0
            self.indexf = 0
        elif self.index == len(self.words):
            if True: #Si ese argumento era necesario
                #Sería como un costo infinito
                return
            else: #Si puede suponerlo:
                #Suponer algo provoca un costo
                costo += 1
        
                #Buscar el siguiente
                self.index = 0
                self.indexf += 1
        else:
            word = self.words[self.index]
            if word == self.actual[ARGS][self.indexf]:
                #Fork
                allstates.add(self.fork(COSTOPORIGNORAR))

                #Conservar
                pass

                #Borrar de words
                del self.words[self.index]
            
                #Buscar el siguiente argumento
                self.indexf += 1
                self.index = 0
            else:
                self.index += 1

        allstates.add(self)

    def __str__(self):
        return "\nwords: " + str(self.words) + "\nindex: " + str(self.index) + "\nset: " + str(self.setoffunctions) + "\nactual: " + str(self.actual) + "\n"

    #Return a copy with actual = None
    def fork(self, extracosto):
        copy = State(self.goal)
        copy.words = self.words[:]
        copy.costo = self.costo + extracosto
        copy.setoffunctions = self.setoffunctions.copy()
        return copy
        
"""
class AllProcs:
    def __init__(self):
        #Lista de (costo, funcion, argumentos) mas importante.
        #Es una lista ordenada de "procesos", los del final son los que tienen
        #un costo mas bajo.
        self.procs = []
        self.lastcosto = 0
        self.outputs = []

    def add(costorelativo, funcion, *args):
        self.procs.append((ultimocosto + costorelativo, funcion, args))

        #Hundir
        for i in reversed(range(len(self.procs) - 1)):
            if (self.procs[i][0] < self.procs[i + 1][0]):
                #Swap
                temp = self.proc[i]
                self.procs[i] = self.procs[i + 1]
                self.procs[i + 1] = temp
            else:
                break
        
    #Execute the cheapest proc.
    #Ejecuta el proceso menos costoso
    #A la vez, lo popea
    #NO Retorna True si debe hacer un "cut" al backtracking
    #Pre: self.procs no vacio
    def execute(self):
        assert len(self.procs) != 0

        last = self.procs.pop() #Popea
        last[1](*last[2]) #Ejecuta
        self.lastcosto = last[0]
        return (lastcosto < 0) #Retorna

        
    def generator(self, words):
        while len(self.procs) > 0:
            execute()
            #if execute():
            #    break
            while len(outputs) > 0:
                yield self.outputs.pop()
"""
