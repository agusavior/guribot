# -*- coding: utf-8 -*-

from threading import Timer
from datetime import datetime
from OPT import HORA, BIENVENIDO
import todatetimes
from utiles import amigos, bot, maysend, informarAdmins
import requests
import sys

def ejecutar(recordatorio, bot, amigo, missed = False):
    #Ver si el amigo sigue teniendo el recordatorio por las
    #dudas ya se haya ejecutado o se haya cancelado.
    #Lo de cancelarse puede pasar si el hilo ya se mando y en ese periodo
    #el usuario lo cancelo.
    if recordatorio in amigo.recordatorios:
        extra = ""
        if missed:
            extra = " Por algun motivo este aviso se atrasó. Lo siento."
    
        #Aqui debe ejecutarlo propiamente dicho
        try:
            bot.reply_to(recordatorio.message, "Recuerda esto." + extra)
            amigo.recordatorios.remove(recordatorio)
        except requests.exceptions.ConnectionError as e:
            print "Problema de conexion al intentar recordar"

def reportestostr(reportes):
    retorno = ""
    for r in reportes:
        retorno += r + ". "
    return retorno
    
class Recordatorio:
    def __init__(self, message, datetime, chatid):
        self.message = message
        self.datetime = datetime
        self.chatid = chatid

    #Si queda menos de una hora, crea un temporizador.
    def setear(self, bot, amigo):
        UTFDIF = -3
        seg = (self.datetime - datetime.now() + UTFDIF * 3600).total_seconds()
        if seg < 1:
            pass
            #ignora el mensaje
            #ejecutar(self, bot, amigo, True)
        if seg > HORA:
            pass #No hacer aun
        else:
            (Timer(seg, ejecutar, [self, bot, amigo])).start()
            
class Amigo:
    def __init__(self, chatid, username):
        self.chatid = chatid
        self.username = username
        self.recordatorios = []
        self.deudas = []
        self.listas = []
        self.ayudas = 0
        self.opciones = []

    def escuchar(self, bot, message, text):
        assert message != None
        replyto = message.reply_to_message
        
        if replyto != None:
            newlist = []
            oldlen = len(self.recordatorios)
            for r in self.recordatorios:
                if r.message.message_id != replyto.message_id:
                    newlist.append(r)
            self.recordatorios = newlist
            newlen = len(newlist)
            maysend(self.chatid, "Se borraron " + str(oldlen - newlen) + " recordatorios")
        else:
            text = text.encode('utf8')
            datatimes = []
            reportes = []
            if False:
                datatimes, reportes = todatetimes.to_list_of_datetime(text)
            else:
                try:
                    datatimes, reportes = todatetimes.to_list_of_datetime(text)
                except Exception as e:
                    informe = "Fallo el todatatimes de: " + text + "\n" + str(e.__doc__)
                    print informe
                    informarAdmins(informe)

            oldlen = len(self.recordatorios)
            for d in datatimes:
                r = Recordatorio(message, d, self.chatid)
                r.setear(bot, self)
                self.recordatorios.append(r)
            newlen = len(self.recordatorios)
            reportes.append("Se agregaron " + str(newlen - oldlen) + " recordatorios")
            maysend(self.chatid, reportestostr(reportes), message = message)

def amigoFromMessage(message):
    chatid = message.chat.id
    username = message.chat.username
    for a in amigos:
        if a.chatid == chatid:
            return a
    if username == None:
        username = "chatid" + str(chatid)
    a = Amigo(chatid, username)
    maysend(chatid, BIENVENIDO, "Fallo en darle la bienvenida a " + username)
    amigos.append(a)
    return a
