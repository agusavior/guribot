git config --global user.name "Agustin Orlando"
git config --global user.email "agusavior@hotmail.com"
echo "inicio.sh dice: Configurado!"

cd ~/guribot
mv save.p save2.p
echo "inicio.sh dice: Cambiado save.p por save2.p"

git pull
echo "inicio.sh dice: Pulleado!"

git reset --hard origin/master
echo "inicio.sh dice: Reseteado"

rm save.p
echo "inicio.sh dice: Eliminado save.p"

mv save2.p save.p
echo "inicio.sh dice: Cambiado save2.p por save.p"

gnome-terminal -e "python servidor.py"
