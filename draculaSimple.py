# -*- coding: cp1252 -*-
N = 20 #Maximo de proposciones atomicas
Z = N**5 #Maximo numero de lineas que tiene la lista de posibilidades
pos = [] #Llena de listas de longitud N de ints
NOSE = 2

ATOMIC = 0
NOT = 1
AND = 2

conocNulo = []
for i in range(N):
    conocNulo.append(NOSE)

class Form:
    pass

class Var:
    pass

class Letter(Var):
    def __init__(char):
        self.char = char

class Constant(Var):
    def __init__(value):
        self.value = value

class P(Form):
    def __init__(self, i):
        self.i = i #Indica el tipo 
        #self.var = var #Tipo Var
        self.type = ATOMIC

class Not(Form):
    def __init__(self, f):
        self.f = f
        self.type = NOT

class And(Form):
    def __init__(self, f1, f2):
        self.f1 = f1
        self.f2 = f2
        self.type = AND

class ParaTodo(Form):
    def __init__(self, letter, termino, func):
        pass

#toma la formula form como una funcion que depende de letter, luego
#cambia letter por el valor value dado
def Evalue(form, letter, value):
    pass

def Or(f1, f2):
    return Not(And(Not(f1), Not(f2)))

def Eq(f1, f2):
    return And(Not(And(f1, Not(f2))), Not(And(Not(f1), f2)))

def MuchosAnd(n):
    if n == 0:
        return P(0)
    else:
        return And(P(n), MuchosAnd(n - 1))
    
def isEqual(original, toOriginal, form, conoc, to):
    #conoc es una lista de longitud N con
    #1, 0, o 9.
    print "se verifica: " + formToString(form)
    
    if (form.type == ATOMIC):
        if (conoc[form.i] == int(to)):
            return True
        elif (conoc[form.i] == int(not to)):
            return False
        else:
            #Esto quiere decir que no es facil saber.
            global cursor
            conoc[form.i] = 1
            if (isEqual(original, toOriginal, original, conoc, toOriginal)):
                #print "Nueva Tabla"
                conocClone = conoc[:]
                conocClone[form.i] = 0
                ret = isEqual(original, toOriginal, original, conocClone, toOriginal)
                return ret
            else:
                return False
            #Se verifico para = 1 y para = 0, si en
            #cualquiera de los dos casos termina dando tautologia
            #implica que es una tautologia.
            #O sea: si A => B
            #y �A => B... => B
    elif (form.type == NOT):
        return isEqual(original, toOriginal, form.f, conoc, not to)
    elif (form.type == AND):
        if (to):
            return (isEqual(original, toOriginal, form.f1, conoc, True) and isEqual(original, toOriginal, form.f2, conoc, True))
        else:
            return (isEqual(original, toOriginal, form.f1, conoc, False) or isEqual(original, toOriginal, form.f2, conoc, False))

def primerNumero(string):
    ret = ""
    for c in string:
        if c=='0' or c=='1' or c=='2' or c=='3' or c=='4' or c=='5' or c=='6' or c=='7' or c=='8' or c=='9':
            ret += c
        else:
            break

    return int(ret)

def buscarArgumento(num, string):
    for i in range(len(string)):
        if string[i] == ' ':
            num -= 1
            if num == 0:
                return i + 1
        if string[i] == '&' or string[i] == 'o' or string[i] == '>' or string[i] == '=':
            num += 2

def stringToForm(string):
    for i in range(len(string)):
        if string[i] == 'p':
            return P(primerNumero(string[i+1:]))
        if string[i] == '�':
            return Not(stringToForm(string[i+1:]))
        if string[i] == '&':
            return And(stringToForm(string[i+1:]), stringToForm(string[buscarArgumento(2, string[i+1:]):]))
        if string[i] == 'o':
            return Or(stringToForm(string[i+1:]), stringToForm(string[buscarArgumento(2, string[i+1:]):]))
        if string[i] == '=':
            return Eq(stringToForm(string[i+1:]), stringToForm(string[buscarArgumento(2, string[i+1:]):]))
        if string[i] == '>':
            return Or(Not(stringToForm(string[i+1:])), stringToForm(string[buscarArgumento(2, string[i+1:]):]))
    print "Caracter no reconocido"

def formToString(form):
    if (form.type == ATOMIC):
        return "p" + str(form.i)
    elif (form.type == NOT):
        return "�" + formToString(form.f)
    elif (form.type == AND):
        return "& " + formToString(form.f1) + " " + formToString(form.f2)

formula0 = And(P(1), Not(P(1)))
formula1 = Or(P(1), Not(P(1)))
formula2 = MuchosAnd(10)
formula3 = stringToForm("�o & & p1 �p2 & p3 �p4 o p2 p5")
formula4 = stringToForm("> & & p1 p2 & p3 p4 o �p1 �p2")
formula5 = stringToForm("o = = p1 p2 & > p3 p4 > p5 p6 = p4 & �p5 �o p7 & & p4 p10 & �p9 p8")
formula6 = stringToForm("& p1 �p2")
temp = formula6
tempBool = False
print "Se evalula: " + formToString(temp)
print isEqual(temp, tempBool, temp, conocNulo, tempBool)
