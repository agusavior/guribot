# -*- coding: utf-8 -*-

from telebot import types
from threading import Timer
from utiles import amigos, bot, guardar, cargar, cerrar, informarAdmins, datetostr
from OPT import HORA, SEGESPERROR, ADMINS, BIENVENIDO
from amigo import amigoFromMessage
import re
import requests
from time import sleep

REABRIR = True #Intentar reabrir si ocurrio un error
numCE = 0 #Cantidad de Connection Error ocurrido
MAXnumCE = 500
timer = None
intencionDeCerrarse = False
    
#Cada una hora se ejecuta esta funcion
def hora():
    guardar()
    for a in amigos:
        for r in a.recordatorios:
            r.setear(bot, a)
    global timer
    timer = Timer(HORA, hora)
    timer.start()

def poll():
    global numCE
    #bot.polling(none_stop=True)
    while not intencionDeCerrarse and numCE < MAXnumCE:
        try:
            bot.polling(none_stop=True)
        except requests.exceptions.ConnectionError as e:
            numCE += 1
            print e
            if not REABRIR:
                return
        except requests.exceptions.ReadTimeout as e:
            numCE += 1
            print e
            if not REABRIR:
                return
        sleep(SEGESPERROR)

def init():
    cargar()
    print("Inicia Thread HORA")
    hora()
    poll()

@bot.message_handler(commands=['start', 'imnew', 'help', 'exit', 'save', 'load', 'info', 'clear'])
def send_command(message):
    isadmin = message.chat.username in ADMINS
    amigo = amigoFromMessage(message)
    response = ""

    if message.text == "/start" or message.text == "/imnew":
        response = BIENVENIDO
    elif message.text == "/save" and isadmin:
        guardar()
        response = "Guardado"
    elif message.text == "/load" and isadmin:
        cargar()
        response = "Cargado"
    elif message.text == "/exit" and isadmin:
        global intencionDeCerrarse
        intencionDeCerrarse = True
        cerrar(timer)
        response = "Cerrado"
    elif message.text == "/help":
        response = BIENVENIDO
    elif message.text == "/info" and isadmin:
        response = "Cantidad de amigos: " + str(len(amigos)) + '\n'
        response += "Cantidad de ConnectionErrors: " + str(numCE) + '\n'
        for a in amigos:
            response += "@" + a.username.encode('utf8') + '\n'
            for r in a.recordatorios:
                response += datetostr(r.datetime) + '\n'
    elif message.text == "/info" and not isadmin:
        for r in amigo.recordatorios:
            response += datetostr(r.datetime) + '\n'
    elif message.text == "/clear":
        amigo.recordatorios = []
    else:
        response = "Comando mal usado. Puede que se deba a que no sos administrador del bot."
    
    bot.send_message(amigo.chatid, response)

@bot.message_handler(func=lambda message: message.text[0] != '/')
def echo_all(message):
    amigo = amigoFromMessage(message)
    amigo.escuchar(bot, message, message.text)
    #bot.send_message(amigo.chatid,  
    #bot.reply_to(message, message.text + "...")

if __name__ == "__main__":
    init()
