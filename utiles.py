# -*- coding: utf-8 -*-

from OPT import ADMINS, HORA, BIENVENIDO, ARCHIVO, TOKEN
import pickle
import os
import sys
import telebot
from datetime import datetime, timedelta

amigos = []
bot = telebot.TeleBot(TOKEN, threaded=False)

def informarAdmins(message):
    try:
        for a in amigos:
            if a.username in ADMINS:
                bot.send_message(a.chatid, message)
    except requests.exceptions.ConnectionError as e:
        print "Problema al informar cosas a los admins."

def maysend(chatid, text, error = "", message = None):
    try:
        if message == None:
            bot.send_message(chatid, text)
        else:
            bot.reply_to(message, text)
    except requests.exceptions.ConnectionError as e:
        informe = str(e) + ". " + errors
        print informe
        informarAdmins(informe)

def datetostr(datetime):
    temp = str(datetime)
    temp2 = str(datetime.now())
    temp3 = str(datetime.now() + timedelta(days=1))
    temp4 = str(datetime.now() + timedelta(days=2))
    retorno = ""
    if temp[0:10] == temp2[0:10]:
        retorno = "hoy"
    elif temp[0:10] == temp3[0:10]:
        retorno = "mañana"
    elif temp[0:10] == temp4[0:10]:
        retorno = "pasado mañana"
    else:
        retorno = temp[8:10] + "/" + temp[5:7] + "/" + temp[0:4]
    retorno += " a las " + temp[11:16] + "hs"
    return retorno

def guardar():
    #Guarda una lista de amigos
    pickle.dump(amigos, open(ARCHIVO, "wb"), protocol=2)

def cerrar(*timers):
    guardar() #Guarda cada vez que cierra
    for t in timers:
        if t != None:
            t.cancel()
    bot.stop_polling()
    
def cargar():
    #Vacia por las dudas
    while amigos != []:
        amigos.pop()

    #Recarga
    if os.path.isfile(ARCHIVO) and os.stat(ARCHIVO).st_size > 0:
        sys.stdout.write("Cargando...")
        amigos.extend(pickle.load(open(ARCHIVO, "rb")))
        sys.stdout.write(" Listo\n")
    else:
        print("ARCHIVO no existe o esta vacio")
    
    for a in amigos:
        for r in a.recordatorios:
            r.setear(bot, a)
