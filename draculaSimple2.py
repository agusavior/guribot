import time

string0 = "> A,ax,Axiomas ax = t = A,i,15 p,x,i & A,i,10 p,x,i A,i,10-15 p,x,i"
string = "A,ax,Axiomas ax" #A,h,H ~ E.Jesus.h"
ayuda = False
espaciosAyuda = 1
entrada = str.split(string)
conectivos = ['&', 'o', '>', '=']

def dictarLista(lista, obligado = False):
    global ayuda
    if ayuda or obligado:
        final = ""
        for l in lista:
            #final += str(l)[0]
            final += str(l)
            for i in range(espaciosAyuda - len(str(l))):
                final += ' '
        print final
    
def dictar(string):
    global ayuda
    if ayuda:
        print string

#Retorna un conjunto de strings
def stringToSet(string):
    if string[0] == 'A':
        return ["~ A,p,P ~ C.p", #Existe al menos un culpable
                "A,p,P > C.p T.p.Arma", #Todo culpable tiene un arma
                "~ C.Jesus", #Jesus siempre es inocente
                "A,p,P ~ A,h,H ~ E.p.h", #Toda persona esta en una habitacion
                ]
    elif string[0] == 'P':
        return ["Jose",
                "Luis",
                "Agus",
                "Marcos",
                "Flor",
                "Jesus",
            ]
    elif string[0] == 'H':
        return ["Comedor",
                "Patio",
                "Sotano",
                "Pieza",
                "Garage",
            ]
    elif string[0] == 'O':
        return ["Arma",
                "Llave",
                "Lapiz",
                "Dinero",
                "Carta",
            ]
    elif '-' in string:
        i = string.index('-')
        return range(int(string[:i]),int(string[i+1:]) + 1)
    else:
        return range(int(string) + 1)

#Devuelve una lista con los indices de donde se encuentran
#los argumentos de la funcion situada en la posicion l
def indicesArgumentales(entrada, l):
    recuerdo = []

    i = len(entrada) - 1
    while i > l:
        if entrada[i] == '~':
            pass
        elif entrada[i] == '&':
            recuerdo.pop()
        else:
            recuerdo.append(i + 1)
        i -= 1
    recuerdo.append(l + 1)
    
    return recuerdo

def descuantificar(entrada):
    for i in reversed(range(len(entrada))):
        if entrada[i][0] == 'A' and entrada[i][1] == ',':
            partes = str.split(entrada[i].replace(',', ' '))
            #parte 1 es el indice
            #parte 2 es el conjunto
            equivalencia = []
            conjunto = stringToSet(partes[2])

            for j in range(len(conjunto) - 1):
                equivalencia.append('&')

            for j in conjunto:
                equivalencia += subForm(entrada, i + 1, partes[1], str(j))
            #print entrada[:i] + equivalencia + entrada[i + 1:]

            return descuantificar(entrada[:i] + equivalencia + entrada[i + 1 + len(subForm(entrada, i + 1)):])
    return entrada

#retorna toda la formula situada en un lugar
#Pre: no hay cuantificadores
#ademas, se debe pasar un indice y un valor particual
def subForm(entrada, lugar, indice = ' ', particular = ' '):
    global conectivos
    
    retorno = []
    args = 1
    while args > 0:
        c = entrada[lugar]
        if c in conectivos:
            args += 1
        elif c == '~':
            pass
        else:
            args -= 1
        retorno.append(c.replace(indice, particular))
        lugar += 1

    return retorno

#Rellena las listas refder y refizq
def refGen(entrada, refder, refizq, refAtomic):
    global conectivos
    
    recuerdo = []
    
    for i in range(len(entrada)):
        refder.append(0)
        refAtomic.append(0)

    refizq.extend(range(-1, len(entrada) - 1))
        
    i = len(entrada) - 1
    while i >= 0:
        if entrada[i] == '~':
            pass
        elif entrada[i] in conectivos:
            refder[i] = recuerdo.pop()
            refizq[refder[i]] = i
        else:
            recuerdo.append(i + 1)
            j = i + 1
            while j < len(entrada):
                if entrada[i] == entrada[j]:
                    refAtomic[i] = j
                    break
                j += 1
        i -= 1

    

#Dado un conectivo y un valor de verdad B, dice si es importante calcular A
#si se tiene <conectivo> A B, para saber <conectivo>
#Pre: vs es 0 o 1
#Pre: vs esta en conectivos
def importa(c, vs):
    if c == '&':
        return vs
    elif c == 'o' or c == '>':
        return 1 - vs
    elif c == '=':
        return 1 #Siempre es importante
    else:
        print "error0" + c

#calcula c a b
#Pre: a y b son bools, c es un char
def valor(c, a, b):
    if c == '&':
        return a and b
    elif c == 'o':
        return a or b
    elif c == '>':
        return not a or b
    elif c == '=':
        return a == b
    else:
        print "error1"
    
def isTau(entrada):
    global conectivos

    #Encerrar todo dentro de un And con un true
    #el algoritmo va a empezar desde len(entrada) - 2 (antes del t)
    entrada.insert(0, '&')
    entrada.append('t')

    entrada = descuantificar(entrada)

    dictarLista(entrada, True)
    
    cursor = len(entrada)
    valores = []   #para ir guardando los valores que se saben
    consid = [cursor - 1,]    #para considerar el segundo valor de las var atomicas
    refder = []
    refizq = []
    refAtomic = []
    refGen(entrada, refder, refizq, refAtomic)

    for i in range(cursor):
        valores.append(0)

    dictarLista(refizq)
    dictarLista(refder)
    dictarLista(refAtomic)

    #Ahora ya hay consideraciones, asi que hay que analizar cada una
    while consid != []:
        cursor = consid.pop()
        valores[cursor] = 1 - valores[cursor]
        while cursor > 0:
            cursor -= 1
            c = entrada[cursor]
            vs = valores[cursor + 1]

            if c in conectivos:
                der = valores[refder[cursor]]
                if der == 2:
                    valores[cursor] = 2
                else:
                    valores[cursor] = int(valor(c, bool(vs), bool(der)))
            elif vs == 2:
                valores[cursor] = 2
            elif c == 't':
                valores[cursor] = 1
            elif c == 'f':
                valores[cursor] = 0
            elif c == '~':
                valores[cursor] = 1 - vs
            else:
                if not importa(entrada[refizq[cursor + 1]], vs):
                    valores[cursor] = 2
                else:
                    #Buscar primero si se conoce el valor
                    valores[cursor] = 0
                    consid.append(cursor)

                    i = refAtomic[cursor]
                    while i != 0:
                        if valores[i] != 2:
                            valores[cursor] = valores[i]
                            consid.pop() #Dejar de considerarlo
                            #recien se metio, ahora se debe sacar, por eso
                            #se usa pop()
                        i = refAtomic[i]
                        

        dictarLista(valores)

        if valores[0] == 0:
            return 0
    return 1

start_time = time.time()
print str(isTau(entrada))
print("--- %s seconds ---" % (time.time() - start_time))

while True:
    print str(isTau(raw_input()))
